package com.luxoft.dnepr.courses.regular.unit3;

import org.junit.Assert;
import org.junit.Test;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * User: cool04ek
 * Date: 24.04.13
 * Time: 11:17
 * To change this template use File | Settings | File Templates.
 */
public class WalletTest {

    private Wallet wallet1 = new Wallet((long) 1, new BigDecimal(100.0), new BigDecimal(15000.00), WalletStatus.ACTIVE);
    private Wallet wallet2 = new Wallet((long) 1, new BigDecimal(1200.0), new BigDecimal(150000.00), WalletStatus.ACTIVE);
    private Wallet walletBlocked = new Wallet((long) 1, new BigDecimal(100.0), new BigDecimal(15000.00), WalletStatus.BLOCKED);

    @Test
    public void withdrawalTest() {
        BigDecimal sum=new BigDecimal(100.0);
        try {
            wallet1.checkWithdrawal(sum);
            wallet2.checkTransfer(sum);
            wallet1.withdraw(sum);
            wallet2.transfer(sum);
            walletBlocked.checkWithdrawal(sum);
        } catch (WalletIsBlockedException | InsufficientWalletAmountException|LimitExceededException ex) {
            ex.printStackTrace();
        }
        Assert.assertEquals(new BigDecimal(1300.0), wallet2.getAmount());
        Assert.assertEquals(new BigDecimal(0.0), wallet1.getAmount());
        wallet1.setAmount(new BigDecimal(99.0));
        try {
            wallet1.checkWithdrawal(sum);
            wallet2.checkTransfer(sum);
            wallet1.withdraw(sum);
            wallet2.transfer(sum);
            walletBlocked.checkWithdrawal(sum);
        } catch (WalletIsBlockedException | InsufficientWalletAmountException|LimitExceededException ex) {
            ex.printStackTrace();
        }
        Assert.assertEquals(new BigDecimal(1300.0), wallet2.getAmount());
        Assert.assertEquals(new BigDecimal(99.0), wallet1.getAmount());
    }
}
