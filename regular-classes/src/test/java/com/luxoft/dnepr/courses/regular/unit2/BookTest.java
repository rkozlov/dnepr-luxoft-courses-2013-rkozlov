package com.luxoft.dnepr.courses.regular.unit2;

import junit.framework.Assert;
import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();
        book = productFactory.createBook("code", null, 200, new GregorianCalendar(2006, 0, 1).getTime());
        cloned = (Book)book.clone();
        book = productFactory.createBook(null, "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        cloned = (Book)book.clone();
    }

    @Test
    public void testEquals() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book cloned = (Book)book.clone();
        assertTrue(book.equals(cloned));
        cloned.setPrice(300);
        assertTrue(book.equals(cloned));
        cloned.setCode("edoc");
        assertFalse(book.equals(cloned));
        Beverage beverage=productFactory.createBeverage("code", "Pepsi", 5.45, true);
        Beverage clonedBev=(Beverage)beverage.clone();
        assertTrue(beverage.equals(clonedBev));
    }
}
