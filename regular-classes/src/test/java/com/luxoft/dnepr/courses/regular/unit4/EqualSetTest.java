package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.*;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cool04ek
 * Date: 28.04.13
 * Time: 18:15
 * To change this template use File | Settings | File Templates.
 */
public class EqualSetTest {

    private  EqualSet equalSet;

    @Before
    public void initSet(){
        equalSet=new EqualSet();
    }

    @Test
    public void objectsTest(){
        BigDecimal bigDc=new BigDecimal(100);
        equalSet.add(bigDc);
        Assert.assertEquals(equalSet.size(), 1);
        equalSet.remove(bigDc);
        Assert.assertEquals(equalSet.size(), 0);
    }

    @Test
    public void arrayTest(){
        equalSet.add("123");
        equalSet.add("456");
        equalSet.add("789");
        equalSet.add("123");
        equalSet.add(null);
        equalSet.add("012");
        Assert.assertEquals(5, equalSet.size());
        equalSet.add(null);
        Assert.assertEquals(5, equalSet.size());

        Assert.assertEquals("123", equalSet.toArray()[0]);
        Assert.assertEquals(null, equalSet.toArray()[3]);
    }

    @Test
    public void iteratorTest(){
        equalSet.add(new BigDecimal(100));
        equalSet.add(new BigDecimal(200));
        equalSet.add(new BigDecimal(300));
        Iterator iter=equalSet.iterator();
        while(iter.hasNext()){
            Assert.assertTrue(equalSet.contains(iter.next()));
        }
    }

    @Test
    public void ctorTest(){
        List<String> collectionTest=new ArrayList();
        collectionTest.add("123");
        collectionTest.add("456");
        collectionTest.add("789");
        EqualSet setTest=new EqualSet(collectionTest);
        Assert.assertEquals("123", setTest.toArray()[0]);
        Assert.assertEquals("456", setTest.toArray()[1]);
        Assert.assertEquals("789", setTest.toArray()[2]);
    }

    @Test
    public void containsTest(){
        equalSet.add("123");
        equalSet.add("456");
        equalSet.add("789");
        equalSet.add("123");
        equalSet.add(null);
        equalSet.add("012");
        Assert.assertEquals(true, equalSet.contains("123"));
        Assert.assertEquals(true, equalSet.contains(null));
        Assert.assertEquals(false, equalSet.contains("1234"));
    }

}
