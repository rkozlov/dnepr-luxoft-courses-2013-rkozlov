package com.luxoft.dnepr.courses.regular.unit3;

import org.junit.Assert;
import org.junit.Test;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
/**
 * Created with IntelliJ IDEA.
 * User: cool04ek
 * Date: 24.04.13
 * Time: 13:24
 * To change this template use File | Settings | File Templates.
 */
public class BankTest {

    private Bank bank=new Bank(System.getProperty("java.version"));
    private Map<Long, UserInterface> users=new HashMap<Long, UserInterface>();
    private WalletInterface wallet1 = new Wallet((long) 1, new BigDecimal(100.0), new BigDecimal(15000.00), WalletStatus.ACTIVE);
    private WalletInterface wallet2 = new Wallet((long) 2, new BigDecimal(1200.0), new BigDecimal(150000.00), WalletStatus.ACTIVE);
    private WalletInterface walletBlocked = new Wallet((long) 3, new BigDecimal(100.0), new BigDecimal(15000.00), WalletStatus.BLOCKED);
    private User u1=new User(wallet1.getId(), "Roma", wallet1);
    private User u2=new User(wallet2.getId(), "Anton", wallet2);
    private User u3=new User(walletBlocked.getId(), "Alex", walletBlocked);


    @Test
    public void moneyTransferTest(){
        users.put(u1.getId(), u1);
        users.put(u2.getId(), u2);
        bank.setUsers(users);
        bank.addUser(u3);
        BigDecimal sum=new BigDecimal(100.0);
        try{
            bank.makeMoneyTransaction((long) 1, (long) 2, sum);
        }catch (TransactionException|NoUserFoundException ex){
            ex.printStackTrace();
        }
        Assert.assertEquals(new BigDecimal(0.0), bank.getUsers().get((long)1).getWallet().getAmount());
        Assert.assertEquals(new BigDecimal(1300.0), bank.getUsers().get((long)2).getWallet().getAmount());
        bank.getUsers().get((long)1).getWallet().setAmount(new BigDecimal(99.0));
        try{
            bank.makeMoneyTransaction((long) 1, (long) 2, sum);
        }catch (TransactionException|NoUserFoundException ex){
            ex.printStackTrace();
        }
        Assert.assertEquals(new BigDecimal(99.0), bank.getUsers().get((long)1).getWallet().getAmount());
        Assert.assertEquals(new BigDecimal(1300.0), bank.getUsers().get((long)2).getWallet().getAmount());
        bank.getUsers().get((long)1).getWallet().setAmount(new BigDecimal(199.445));
        try{
            bank.makeMoneyTransaction((long) 1, (long) 3, sum);
        }catch (TransactionException|NoUserFoundException ex){
            ex.printStackTrace();
        }
        Assert.assertEquals(new BigDecimal(199.445), bank.getUsers().get((long)1).getWallet().getAmount());
        Assert.assertEquals(new BigDecimal(1300.0), bank.getUsers().get((long)2).getWallet().getAmount());
        try{
            bank.makeMoneyTransaction((long) 1, (long) 6, sum);
        }catch (TransactionException|NoUserFoundException ex){
            ex.printStackTrace();
        }
        Assert.assertEquals(new BigDecimal(199.445), bank.getUsers().get((long)1).getWallet().getAmount());
        bank.getUsers().get((long)2).getWallet().setMaxAmount((new BigDecimal(1399.446)));
        try{
            bank.makeMoneyTransaction((long) 1, (long) 2, sum);
        }catch (TransactionException|NoUserFoundException ex){
            ex.printStackTrace();
        }
        Assert.assertEquals(new BigDecimal(199.445), bank.getUsers().get((long)1).getWallet().getAmount());
        Assert.assertEquals(new BigDecimal(1300.0), bank.getUsers().get((long)2).getWallet().getAmount());
        bank=new Bank(System.getProperty("java.version"));
        try{
            bank.makeMoneyTransaction((long) 1, (long) 2, sum);
        }catch (TransactionException|NoUserFoundException ex){
            ex.printStackTrace();
        }
    }

}
