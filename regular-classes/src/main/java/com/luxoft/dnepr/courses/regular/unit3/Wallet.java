package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * Created with IntelliJ IDEA.
 * User: cool04ek
 * Date: 23.04.13
 * Time: 19:27
 * To change this template use File | Settings | File Templates.
 */
public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount, maxAmount;
    private WalletStatus status;

    public Wallet(Long id, BigDecimal amount, BigDecimal maxAmount, WalletStatus status){
        this.id=id;
        this.amount=amount;
        this.maxAmount=maxAmount;
        this.status=status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public WalletStatus getStatus() {
        return status;
    }

    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (getStatus() == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(this.id, "");
        }
        if (this.amount.compareTo(amountToWithdraw) < 0) {
            throw new InsufficientWalletAmountException(this.id, amountToWithdraw, this.amount, "");
        }
    }

    public void withdraw(BigDecimal amountToWithdraw) {
        this.amount = this.amount.subtract(amountToWithdraw);
    }

    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (getStatus() == WalletStatus.BLOCKED) {
            throw new WalletIsBlockedException(this.id, "");
        }
        if (this.amount.add(amountToTransfer).compareTo(this.maxAmount) > 0) {
            throw new LimitExceededException(this.id, amountToTransfer, this.amount, "");
        }
    }

    public void transfer(BigDecimal amountToTransfer) {
        this.amount = this.amount.add(amountToTransfer);
    }
}
