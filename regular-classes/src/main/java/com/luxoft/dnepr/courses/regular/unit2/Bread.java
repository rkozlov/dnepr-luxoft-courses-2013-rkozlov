package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct implements Product{

    private double weight;

    public Bread() {}

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bread bread = (Bread) o;

        if (Double.compare(bread.weight, weight) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        long temp = weight != +0.0d ? Double.doubleToLongBits(weight) : 0L;
        return (int) (temp ^ (temp >>> 32));
    }

}
