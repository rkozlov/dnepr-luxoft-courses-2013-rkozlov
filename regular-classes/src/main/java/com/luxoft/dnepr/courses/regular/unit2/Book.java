package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct implements Product {

    private Date publicationDate;


    public Date getDate() {
        return publicationDate;
    }

    public void setDate(Date date) {
        this.publicationDate = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        if (publicationDate != null ? !publicationDate.equals(book.publicationDate) : book.publicationDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (publicationDate != null ? publicationDate.hashCode() : 0);
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Book newObject=(Book) super.clone();
        newObject.publicationDate = (Date)this.publicationDate.clone();
        return newObject;
    }
}
