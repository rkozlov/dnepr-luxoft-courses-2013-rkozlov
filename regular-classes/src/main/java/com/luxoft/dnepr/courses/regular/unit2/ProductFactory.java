package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Calendar;
import java.util.Date;

/**
 * Product factory.
 * Simplifies creation of different kinds of products.
 */
public class ProductFactory {

    public Bread createBread(String code, String name, double price, double weight) {
        Bread bread=new Bread();
        bread.setCode(code);
        bread.setName(name);
        bread.setPrice(price);
        bread.setWeight(weight);
        return bread;
    }

    public Beverage createBeverage(String code, String name, double price, boolean nonAlcoholic) {
        Beverage beverage=new Beverage();
        beverage.setName(name);
        beverage.setCode(code);
        beverage.setPrice(price);
        beverage.setNonAlcoholic(nonAlcoholic);
        return beverage;
    }

    public Book createBook(String code, String name, double price, Date publicationDate) {
        Book book=new Book();
        book.setName(name);
        book.setCode(code);
        book.setPrice(price);
        book.setDate(publicationDate);
        return book;
    }
}
