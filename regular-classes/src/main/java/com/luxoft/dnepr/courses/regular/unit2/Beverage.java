package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct implements Product {

    private boolean nonAlc;

    public Beverage() {}

        public boolean isNonAlcoholic() {
        return this.nonAlc;
    }

    public void setNonAlcoholic(boolean inAlc) {
        this.nonAlc = inAlc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        if (nonAlc != beverage.nonAlc) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nonAlc ? 1 : 0);
        return result;
    }

}
