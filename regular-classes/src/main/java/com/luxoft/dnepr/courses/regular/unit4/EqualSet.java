package com.luxoft.dnepr.courses.regular.unit4;

import com.sun.istack.internal.Nullable;

import java.util.ArrayList;
import java.util.Set;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: cool04ek
 * Date: 27.04.13
 * Time: 13:13
 * To change this template use File | Settings | File Templates.
 */
public class EqualSet<E> implements Set<E> {
    private List<E> objectSet;

    public EqualSet() {
        objectSet = new ArrayList<>();
    }

    public EqualSet(Collection<? extends E> collection) {
        objectSet = new ArrayList<>();
        if (collection != null) {
            for (E e : collection) {
                if (!objectSet.contains(e)) {
                    objectSet.add(e);
                }
            }
        }
    }

    public boolean contains(Object o) {
        for (E e : objectSet) {
            if (e == null ? o == null : e.equals(o)) {
                return true;
            }
        }
        return false;
    }

    public void clear() {
        objectSet.clear();
    }

    public boolean add(E e) {
        if (!objectSet.contains(e)) {
            objectSet.add(e);
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        return objectSet.isEmpty();
    }

    public boolean retainAll(Collection<?> c) {
        return objectSet.retainAll(c);
    }

    public Object[] toArray() {
        return Collections.unmodifiableCollection(objectSet).toArray();
    }


    public <T> T[] toArray(T[] a) {
        return Collections.unmodifiableCollection(objectSet).toArray(a);
    }

    public boolean containsAll(Collection<?> c) {
        return objectSet.containsAll(c);
    }

    public boolean remove(Object o) {
        return objectSet.remove(o);
    }

    public Iterator<E> iterator() {
        return objectSet.iterator();
    }

    public boolean removeAll(Collection<?> c) {
        return objectSet.removeAll(c);
    }

    public boolean addAll(Collection<? extends E> c) {
        boolean res = false;
        if (c == null) {
            return res;
        }
        for (E e : c) {
            if (!objectSet.contains(e)) {
                objectSet.add(e);
                res = true;
            }
        }
        return res;
    }

    public int size() {
        return objectSet.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EqualSet equalSet = (EqualSet) o;

        if (objectSet != null ? !objectSet.equals(equalSet.objectSet) : equalSet.objectSet != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return objectSet != null ? objectSet.hashCode() : 0;
    }
}
