package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;
import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.HashMap;




/**
 * Created with IntelliJ IDEA.
 * User: cool04ek
 * Date: 23.04.13
 * Time: 20:30
 * To change this template use File | Settings | File Templates.
 */
public class Bank implements BankInterface {
    private Map<Long, UserInterface> users;

    public Map<Long, UserInterface> getUsers() {
        return this.users;
    }

    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    public void addUser(UserInterface user){
        this.users.put(user.getId(), user);
    }

    public void revomeUser(UserInterface user){
        this.users.remove(user.getId());
    }

    public Bank(String expectedJavaVersion) throws IllegalJavaVersionError {
        if (!System.getProperty("java.version").equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(System.getProperty("java.version"), expectedJavaVersion, "");
        }
    }

    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        checkMapIsNull();
        if(!this.users.containsKey(fromUserId)){
            throw new NoUserFoundException(fromUserId,"User with id "+fromUserId+" not found");
        }
        if(!this.users.containsKey(toUserId)){
            throw new NoUserFoundException(toUserId,"User with id "+toUserId+" not found");
        }
        try{
            this.users.get(fromUserId).getWallet().checkWithdrawal(amount);
        }catch (WalletIsBlockedException ex){
            throw new TransactionException("User '"+this.users.get(fromUserId).getName()+"' wallet is blocked");
        }catch (InsufficientWalletAmountException ex){
            throw new TransactionException("User '"+this.users.get(fromUserId).getName()+
                    "' has insufficient funds ("+this.users.get(fromUserId).getWallet().getAmount().setScale(2, RoundingMode.HALF_EVEN)+" < "+amount.setScale(2, RoundingMode.HALF_EVEN)+")");
        }
        try{
            this.users.get(toUserId).getWallet().checkTransfer(amount);
            this.users.get(fromUserId).getWallet().withdraw(amount);
            this.users.get(toUserId).getWallet().transfer(amount);
        }catch (WalletIsBlockedException ex){
            throw new TransactionException("User '"+this.users.get(toUserId).getName()+"' wallet is blocked");
        }catch (LimitExceededException ex){
            throw new TransactionException("User '"+this.users.get(toUserId).getName()+"' wallet limit exceeded ("+
                    this.users.get(toUserId).getWallet().getAmount().setScale(2, RoundingMode.HALF_EVEN)+" + "+amount.setScale(2, RoundingMode.HALF_EVEN)+" > "+
                    this.users.get(toUserId).getWallet().getMaxAmount().setScale(2, RoundingMode.HALF_EVEN)+")");
        }
    }
    private void checkMapIsNull() {
        if (this.users==null){
            this.users = new HashMap<Long, UserInterface>();
        }
    }
}
