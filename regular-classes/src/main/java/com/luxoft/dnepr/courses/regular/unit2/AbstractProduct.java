package com.luxoft.dnepr.courses.regular.unit2;

/**
 * Created with IntelliJ IDEA.
 * User: cool04ek
 * Date: 20.04.13
 * Time: 21:51
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractProduct implements Product, Cloneable {

    private double price;
    private String name;
    private String code;


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractProduct that = (AbstractProduct) o;

        if (code != null ? !code.equals(that.code) : that.code != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        AbstractProduct newObject = (AbstractProduct) super.clone();
        // Now one needs to clone all reference fields
        newObject.name = this.name;
        newObject.code = this.code;
        return newObject;
    }

}
