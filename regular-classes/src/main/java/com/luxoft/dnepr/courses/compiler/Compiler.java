package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.lang.*;

public class Compiler {

	public static final boolean DEBUG = true;

	public static void main(String[] args) {
		byte[] byteCode = compile(getInputString(args));
		VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
		vm.run();
	}

	static byte[] compile(String input) {
		ByteArrayOutputStream result = new ByteArrayOutputStream();
        input=input.replaceAll(" ","");
        checkNull(input);
        int n;
        for(int i=0; i<input.length(); i++){
            if(input.charAt(i)=='('){
                String tmpStr=getInBrakets(input, i+1);
                getSimple(tmpStr,result);
                n=tmpStr.length();
                tmpStr=input.substring(0,i);
                if (input.charAt(i+n)!='$'){
                    input=tmpStr+input.substring(i+n+2,input.length());
                }
                else{
                    input=tmpStr;
                    break;
                }
            }
        }
        getSimple(input, result);
        addCommand(result,VirtualMachine.PRINT);
		return result.toByteArray();
	}

    private static String getInBrakets(String str, int pos){
        StringBuilder strInBrakets=new StringBuilder();
        while (str.charAt(pos)!=')'){
            strInBrakets.append(str.charAt(pos));
            pos++;
        }
        return strInBrakets.toString();
    }

    private static void getSimple(String str, ByteArrayOutputStream result){
        if (checkSimple(str)){
            simpleCmnd(str, result);
        }
        else{

        }
    }

    private static boolean checkSimple(String str){
        int count=0, i;
        for(i=0; i<str.length();i++){
            if(str.charAt(i)=='+' || str.charAt(i)=='-' || str.charAt(i)=='*' || str.charAt(i)=='/'){
                count++;
            }
        }
        return count==1;
    }

    private static void simpleCmnd(String str, ByteArrayOutputStream result){
        int i;
        for(i=0; i<str.length(); i++){
            if(!Character.isDigit(str.charAt(i)) && str.charAt(i)!='.'){
                break;
            }
        }
        if (str.length()!=i+1){
            addCommand(result, VirtualMachine.PUSH, Double.parseDouble(str.substring(i + 1, str.length())));
        }
        if (i!=0){
            addCommand(result, VirtualMachine.PUSH, Double.parseDouble(str.substring(0, i)));
        }
        addCommand(result, commandCase(str.charAt(i)));
    }

    private static byte commandCase(char in){
        switch(in){
            case '+':
                return VirtualMachine.ADD;
            case '-':
                return VirtualMachine.SUB;
            case '*':
                return VirtualMachine.MUL;
            case '/':
                return VirtualMachine.DIV;
        }
        return VirtualMachine.PRINT;
    }

    private static void checkNull(String input){
        for (int i=0; i<input.length(); i++){
            if (!Character.isDigit(input.charAt(i))&&(input.charAt(i) != '+' && input.charAt(i) != '-' &&
                    input.charAt(i) != '*' && input.charAt(i) != '/') && input.charAt(i) != '.' &&
                    input.charAt(i) != '(' && input.charAt(i) != ')'){
                throw new IllegalArgumentException();
            }
        }
    }
	/**
	 * Adds specific command to the byte stream.
	 * @param result
	 * @param command
	 */
	public static void addCommand(ByteArrayOutputStream result, byte command) {
		result.write(command);
	}
	
	/**
	 * Adds specific command with double parameter to the byte stream.
	 * @param result
	 * @param command
	 * @param value
	 */
	public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
		result.write(command);
		writeDouble(result, value);
	}
	
	private static void writeDouble(ByteArrayOutputStream result, double val) {
		long bits = Double.doubleToLongBits(val);
		
		result.write((byte)(bits >>> 56));
		result.write((byte)(bits >>> 48));
		result.write((byte)(bits >>> 40));
		result.write((byte)(bits >>> 32));
		result.write((byte)(bits >>> 24));
		result.write((byte)(bits >>> 16));
		result.write((byte)(bits >>>  8));
		result.write((byte)(bits >>>  0));
	}
	
	private static String getInputString(String[] args) {
		if (args.length > 0) {
			return join(Arrays.asList(args));
		}
		
		Scanner scanner = new Scanner(System.in);
		List<String> data = new ArrayList<String>();
		while (scanner.hasNext()) {
			data.add(scanner.next());
		}
		return join(data);
	}

	private static String join(List<String> list) {
		StringBuilder result = new StringBuilder();
		for (String element : list) {
			result.append(element);
		}
		return result.toString();
	}

}
