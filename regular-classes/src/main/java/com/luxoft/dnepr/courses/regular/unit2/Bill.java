package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    private List<CompositeProduct> compositeProducts=new ArrayList<CompositeProduct>();


    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     * @param product new product
     */
    public void append(Product product) {
        boolean flag=false;
        for(CompositeProduct cmpsProd: compositeProducts){
            if(product.equals(cmpsProd.showExample())){
                cmpsProd.add(product);
                flag=true;
            }
        }
        if(!flag){
            CompositeProduct tmpCompProd=new CompositeProduct();
            tmpCompProd.add(product);
            compositeProducts.add(tmpCompProd);
        }
    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     * @return
     */
    public double summarize() {
        double sumPrice=0.0;
        for(CompositeProduct cmpsProds: compositeProducts){
            sumPrice+=cmpsProds.getPrice();
        }
        return sumPrice;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     * @return
     */
    public List<Product> getProducts() {

        Collections.sort(compositeProducts, new Comparator<CompositeProduct>() {
            @Override
            public int compare(CompositeProduct o1, CompositeProduct o2) {
                double v1=o1.getPrice();
                double v2=o2.getPrice();
                return (v1 < v2) ? 1 : (v1 == v2) ? 0 : -1;
            }
        });
        List<Product> resultList=new ArrayList<Product>();
        for(CompositeProduct cmpsProds: compositeProducts){
            resultList.add(cmpsProds);
        }
        return resultList;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize() ;
    }

}
